﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


namespace ABCUnity.Example
{
    public class BasicLayout : MonoBehaviour
    {
       // [SerializeField] private TextMeshPro title;
       [SerializeField] public string resourceName;
        //public string resourceName;

        //moved it here to make it public
        public  TextAsset abcTextAsset;

        [Range(0.0f, 1.0f)]public float layoutWidth = 0.8f;
        private Camera mainCamera;
        private Layout layout;
        private float aspect;
        private float orthographicSize;

        void Awake()
        {
            mainCamera = Camera.main;
            layout = FindObjectOfType<Layout>();
            
            ResizeLayout();
            layout.onLoaded += OnLoaded;
        }

        void OnLoaded(ABC.Tune tune)
        {
           // title.text = tune.title;
        }

        void Start()
        {
            //commented out for now
            //if (!string.IsNullOrEmpty(resourceName))
            //    LoadFromResource(resourceName);
        }

        void Update()
        {
            //added this to only update when it is there
            if (!string.IsNullOrEmpty(resourceName))
                LoadFromResource(resourceName);

            if (aspect != mainCamera.aspect || orthographicSize != mainCamera.orthographicSize)
                ResizeLayout();
        }

        public void LoadFromResource(string resourceName)
        {
            //TextAsset abcTextAsset = Resources.Load(resourceName) as TextAsset;
            //localised declration
            abcTextAsset = Resources.Load(resourceName) as TextAsset;

            if (abcTextAsset)
                layout.LoadString(abcTextAsset.text);
        }

        private void ResizeLayout()
        {
            aspect = mainCamera.aspect;
            orthographicSize = mainCamera.orthographicSize;

            float orthoHeight = orthographicSize* 2.0f;
            float targetWidth = (orthoHeight * aspect) * layoutWidth;

            //var titleTransform = title.rectTransform;
           // titleTransform.position = new Vector3(0.0f, orthographicSize, 0.0f);
          //  titleTransform.sizeDelta = new Vector2 (targetWidth, titleTransform.sizeDelta.y);

            var layoutSpacer = 1.5f;
            var layoutTransform = layout.GetComponent<RectTransform>();
          //  layoutTransform.position = new Vector3(0.0f, titleTransform.position.y - titleTransform.rect.height - layoutSpacer, 0.0f);
          //  layoutTransform.sizeDelta = new Vector2(targetWidth, orthoHeight - titleTransform.sizeDelta.y - layoutSpacer);
        }
    }

}
