X:1
T:Variations 7
L:1/8
M:4/4
V:1
V:2 clef=bass
K:C
[V:1] z e cA FGFE | DFAB edBA | G E2 F GBdc | _B A G F E^C/2 c c2 :|
[V:2] [D,F,C]8 | [G,,F,B,]8 | [C,E,B,]8 | [A,,G,^C]8 :|