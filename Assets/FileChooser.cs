﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//added this for the button click thing 
using UnityEngine.UI;
using UnityEngine.InputSystem; 
//adding for the simplefilebrowser api following the code sample
using SimpleFileBrowser;
//added to address context of Path
using System.IO;

public class FileChooser : MonoBehaviour
{
    // some notes from the author of the api
    // Warning: paths returned by FileBrowser dialogs do not contain a trailing '\' character
	// Warning: FileBrowser can only show 1 dialog at a time

	//adding a boolean flag here to make it wait 
	public bool isOpen = false; 

    // Start is called before the first frame update
    void Start()
    {
    	//Button btn = yourButton.GetComponent<Button>();
    	//btn.onClick.AddListener(TaskOnClick);
    }//end start

   //putting this for the sake of clicking the filebrowser option
    public void TaskOnClick(){
    	isOpen = true; 
    	 // Set filters (optional)
		// It is sufficient to set the filters just once (instead of each time before showing the file browser dialog), 
		// if all the dialogs will be using the same filters
		FileBrowser.SetFilters( true, new FileBrowser.Filter( "MIDI Files", ".mid", ".midi" ), new FileBrowser.Filter( "Text Files", ".txt", ".pdf" ) );

		// Set default filter that is selected when the dialog is shown (optional)
		// Returns true if the default filter is set successfully
		// In this case, set Images filter as the default filter
		FileBrowser.SetDefaultFilter( ".mid" );

		// Add a new quick link to the browser (optional) (returns true if quick link is added successfully)
		// It is sufficient to add a quick link just once
		// Name: Users
		// Path: C:\Users
		// Icon: default (folder icon)
		FileBrowser.AddQuickLink( "Users", "/Users/jrdndj/Piano2.0v1/Assets/MusicXML", null );

		// Show a select folder dialog 
		// onSuccess event: print the selected folder's path
		// onCancel event: print "Canceled"
		// Load file/folder: folder, Allow multiple selection: false
		// Initial path: default (Documents), Initial filename: empty
		// Title: "Select Folder", Submit button text: "Select"
		// FileBrowser.ShowLoadDialog( ( paths ) => { Debug.Log( "Selected: " + paths[0] ); },
		//						   () => { Debug.Log( "Canceled" ); },
		//						   FileBrowser.PickMode.Folders, false, null, null, "Select Folder", "Select" );

		// Coroutine example
		if(isOpen){
			StartCoroutine( ShowLoadDialogCoroutine() );
			isOpen = false;
		}//endif
   }

    //adding a coroutine based on the sample code
    public IEnumerator ShowLoadDialogCoroutine()
    //changed to cegular functions
    // public void ShowLoadDialogCoroutine()
	{
		// Show a load file dialog and wait for a response from user
		// Load file/folder: both, Allow multiple selection: true
		// Initial path: default (Documents), Initial filename: empty
		// Title: "Load File", Submit button text: "Load"
		yield return FileBrowser.WaitForLoadDialog( FileBrowser.PickMode.FilesAndFolders, true, null, null, "Load Files and Folders", "Load" );
		//this line of code returns the object loaded by the user 

		// Dialog is closed
		// Print whether the user has selected some files/folders or cancelled the operation (FileBrowser.Success)
		Debug.Log( FileBrowser.Success );

		if( FileBrowser.Success )
		{
			// Print paths of the selected files (FileBrowser.Result) (null, if FileBrowser.Success is false)
			for( int i = 0; i < FileBrowser.Result.Length; i++ )
				Debug.Log( FileBrowser.Result[i] );

			// Read the bytes of the first file via FileBrowserHelpers
			// Contrary to File.ReadAllBytes, this function works on Android 10+, as well
			byte[] bytes = FileBrowserHelpers.ReadBytesFromFile( FileBrowser.Result[0] );

			// Or, copy the first file to persistentDataPath
			string destinationPath = Path.Combine( Application.persistentDataPath, FileBrowserHelpers.GetFilename( FileBrowser.Result[0] ) );
			FileBrowserHelpers.CopyFile( FileBrowser.Result[0], destinationPath );
		}
	}
    
    // Update is called once per frame
    void Update()
    {
        
    }
}
