﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
public class PianoInput : MonoBehaviour
{
    
	public InputAction piano;
	
	/* note to self the character controller links are sample references
	   that we can use to manipulate objects
	   in our scenario we will be using the piano elements displayed in 3d 
	   then change their behaviour instead of using the move -- e.g. play some music
	   reference material can be found here: www.youtube.com/watch?v=HhIy23NImdA 
	  */ 
	//public CharacterController controller;

	void Start(){
		//controller = GetComponent<CharacterController>();

	}

	void OnEnable(){
		piano.Enable();
	}

	void OnDisable(){
		piano.Disable();
	}

    // Update is called once per frame
    void Update()
    {
    
    	//commented for now
    //	Vector2 inputVector = piano.ReadValue<Vector2>();
       // Debug.Log(inputVector.ToString());
        //controller.Move(inputVector * Time.deltaTime * 3.14f);
        //use this code to manipulate the keys to actually pressing
    }
}
